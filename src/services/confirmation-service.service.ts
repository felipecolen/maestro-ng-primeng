import { Injectable } from '@angular/core';
import { ConfirmationService as CsPrimeng } from 'primeng/components/common/confirmationservice';
import { ConfirmationModel, IConfirmationService } from '@maestro-ng/core';

@Injectable()
export class ConfirmationService implements IConfirmationService {

  constructor(
    private confirmationService: CsPrimeng
  ) {
  }

  public confirm(model: ConfirmationModel) {
    this.confirmationService.confirm({
      key: 'global',
      header: model.title,
      message: model.message,
      accept: model.accept,
      reject: model.reject
    });
  }
}
