import { Component, Input } from '@angular/core';
import { OverlayPanel } from 'primeng/components/overlaypanel/overlaypanel';
import { Restriction } from '@maestro-ng/core';
import { RestrictionOperator } from '@maestro-ng/core';
import { Operator } from '../../models/operator.interface';

@Component({
  selector: 'm-number-restriction',
  templateUrl: './number-restriction.component.html',
  styleUrls: ['./number-restriction.component.css'],
})
export class NumberRestrictionComponent {

  @Input() public label: string;
  @Input() public restriction: Restriction<number>;

  public operators: Operator[] = [];

  constructor() {
    this.operators.push({ id: RestrictionOperator.IS_NOT_NULL, label: 'Com definição' });
    this.operators.push({ id: RestrictionOperator.EQUALS, label: 'Igual' });
    this.operators.push({ id: RestrictionOperator.GE, label: 'Maior e igual' });
    this.operators.push({ id: RestrictionOperator.GT, label: 'Maior que' });
    this.operators.push({ id: RestrictionOperator.LE, label: 'Menor e igual' });
    this.operators.push({ id: RestrictionOperator.LT, label: 'Menor que' });
    this.operators.push({ id: RestrictionOperator.IN, label: 'Dentro das opções' });
    this.operators.push({ id: RestrictionOperator.BETWEEN, label: 'Entre os limites' });

    this.operators.push({ id: RestrictionOperator.IS_NULL, label: 'Sem definição' });
    this.operators.push({ id: RestrictionOperator.NOT_EQUALS, label: 'Diferente' });
    this.operators.push({ id: RestrictionOperator.NOT_IN, label: 'Fora das opções' });
    this.operators.push({ id: RestrictionOperator.NOT_BETWEEN, label: 'Fora dos limites' });
  }

  public isOperatorTypeBetween(): boolean {
    return this.restriction.isOperatorTypeBetweenOrNotBetween();
  }

  public isOperatorTypeMultiple(): boolean {
    return this.restriction.isOperatorTypeInOrNotIn();
  }

  public isOperatorTypeSingle(): boolean {
    return !this.restriction.isOperatorTypeInOrNotIn()
      && !this.restriction.isOperatorTypeIsNullOrIsNotNull()
      && !this.restriction.isOperatorTypeBetweenOrNotBetween();
  }
}
