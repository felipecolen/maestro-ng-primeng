import { OverlayPanel } from 'primeng/components/overlaypanel/overlaypanel';
import { Component, Input } from '@angular/core';
import { Restriction } from '@maestro-ng/core';
import { RestrictionOperator } from '@maestro-ng/core';
import { Operator } from '../../models/operator.interface';

@Component({
  selector: 'm-string-restriction',
  templateUrl: './string-restriction.component.html',
  styleUrls: ['./string-restriction.component.css'],
})
export class StringRestrictionComponent {

  @Input() public label: string;
  @Input() public restriction: Restriction<string>;

  public operators: Operator[] = [];

  constructor() {
    this.operators.push({ id: RestrictionOperator.IS_NOT_NULL, label: 'Com definição' });
    this.operators.push({ id: RestrictionOperator.EQUALS, label: 'Igual' });
    this.operators.push({ id: RestrictionOperator.CONTAINS, label: 'Contém' });
    this.operators.push({ id: RestrictionOperator.START_WITH, label: 'Começa com' });
    this.operators.push({ id: RestrictionOperator.END_WITH, label: 'Termina com' });
    this.operators.push({ id: RestrictionOperator.IN, label: 'Dentro das opções' });

    this.operators.push({ id: RestrictionOperator.IS_NULL, label: 'Sem definição' });
    this.operators.push({ id: RestrictionOperator.NOT_EQUALS, label: 'Diferente' });
    this.operators.push({ id: RestrictionOperator.NOT_CONTAINS, label: 'Não contém' });
    this.operators.push({ id: RestrictionOperator.NOT_START_WITH, label: 'Não começa com' });
    this.operators.push({ id: RestrictionOperator.NOT_END_WITH, label: 'Não termina com' });
    this.operators.push({ id: RestrictionOperator.NOT_IN, label: 'Fora das opções' });
  }

  public isOperatorTypeMultiple(): boolean {
    return this.restriction.isOperatorTypeInOrNotIn();
  }

  public isOperatorTypeSingle(): boolean {
    return !this.restriction.isOperatorTypeInOrNotIn() && !this.restriction.isOperatorTypeIsNullOrIsNotNull();
  }
}
