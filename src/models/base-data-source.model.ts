import { IMessageService, IProgressService, PagedQueryResult } from '@maestro-ng/core';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';

export class BaseDataSource<E> {

  public entities: E[];
  public lastLoadEvent: LazyLoadEvent;
  public total: number;

  constructor(
    protected messageService: IMessageService,
    protected progressService: IProgressService,
    public rows = 10,
    protected onLazyLoadAction: (event: LazyLoadEvent) => Promise<PagedQueryResult<E>>) {
  }

  public load(): void {
    this.onLazyLoad({ first: 0, rows: this.rows });
  }

  public refresh(): void {
    this.onLazyLoad(this.lastLoadEvent);
  }

  public onEnterSearch(event: any): void {
    if (event.keyCode === 13) {
      this.load();
    }
  }

  public onLazyLoad(event: LazyLoadEvent): void {
    this.progressService.showModeless();
    this.onLazyLoadAction(event)
      .then(result => {
        this.entities = result.entities;
        this.lastLoadEvent = event;
        this.total = result.count;
        this.progressService.hide();
      })
      .catch(result => {
        this.messageService.addError('Erro', 'Erro ao carregar os dados no servidor');
        this.progressService.hide();
      });
  }
}
